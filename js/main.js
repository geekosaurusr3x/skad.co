var ghapi;
var month = new Array(12);
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";

//------------------------------------------------------------------------------
if (typeof ghtoken !== 'undefined') {
    ghapi = new Octokat({
        token: ghtoken
    });
} else {
    ghapi = new Octokat();
}

var repo = ghapi.repos('skad', 'skad.co_content');
var converter = new showdown.Converter({});


function load(page) {
    $('.item[data-name]').removeClass('active');
    $('.item[data-name="' + page + '"]').addClass('active');
    updateBeamcrumb(page);
    repo.contents(page).read()
        .then(function(contents) {
            var htmlConver = converter.makeHtml(contents);
            $('#mdcontent').html(htmlConver);
        });
}

function updateBeamcrumb(page){
  var pages = page.split('/');
  var html = "";
  pages.forEach(function(el,index){
    html+= '<div class="divider">/</div><a class="section">'+el+'</a>';
  });
  $('#breadcrumb').html(html);
}

$('#term').terminal(function(command, term) {
    if (command !== '') {
        try {
            var result = window.eval(command);
            if (result !== undefined) {
                term.echo(result);
            }
        } catch (e) {
            term.error(e);
        }
    } else {
        term.echo('');
    }
}, {
    greetings: '',
    name: 'js_demo',
    height: 200,
    prompt: 'js> '
});

//load
repo.contents.fetch(function(err, message) {
    if (err !== null) {
        console.error(err.message);
    }
    message.forEach(function(el, index) {
        var html;
        var name = el.name;
        if (el.type !== 'dir') {
            html = '<a data-name=' + name + ' class="item">' + name + '</a>';
        } else if (el.type == 'dir' && name !== 'img') {
            html = '<a class="ui dropdown item">' + name + '<i class="dropdown icon"></i>' +
                '<div id="' + name + '" class="menu">';

            el.fetch(function(err, message) {
                var tst = "";
                var divname = this.divname;
                message.forEach(function(el, index) {
                    tst += '<div data-name=' +
                        divname + '/' + el.name + ' class="item">' + el.name + '</div>';
                });
                $('div#' + divname).append(tst);
                $('div.item[data-name]').on('click', function() {
                    load($(this).data('name'));
                });
            }.bind({
                divname: name
            }));

            html += '</div></a>';
        }
        $('#menu_container').append(html);
    });

    $('a.item[data-name]').on('click', function() {
        load($(this).data('name'));
    });

    //activate the ui elements
    $('.ui.dropdown').dropdown();

});

$(document.documentElement).keypress(function(e) {

    e.preventDefault();
    if (e.which == 32) {
        $('#term_box').transition('fly down');
    }
});

//load user infos
ghapi.users('skad').fetch(function(err, user) {
    console.log(user);
    $('#avatargh').attr('src',user.avatarUrl);
    $('#namegh').html(user.name);
    $('#pseudogh').attr('href',user.htmlUrl);
    $('#pseudogh').html(user.login);
    $('#biogh').html(user.bio);
    var date_in = new Date(user.createdAt);
    $('#biogh').html('Joined in '+month[date_in.getMonth()]+" "+date_in.getFullYear());
});

//load main page
load('readme.md');
